# Umsetzungsvorschlag - Modul 319



1. Einführung in Programmablauf mit Activity-Diagramm anhand Alltagsbeispiel (z.B. Kochrezept). Diverse Übungen, um die einfachen Elemente (Start- und Endknoten, Kontrollfluss, Aktivität, Verzweigung mit “Guards”) kennenzulernen.

   * Merkblatt P1 “Activity-Diagramm”

     

2. Einfachen Ablauf in Programmcode übersetzen, dies mit Hilfe von Mustercode. (Mit BlueJ, um Zugang einfach zu machen)
Idee: Addition als ersten Ablauf mit Werteingabe über “Bibliotheksfunktion” (Scanner). Bedeutung von Bibliotheken für Programmierung thematisieren. EVA-Prinzip (so einfach, so wichtig). Bereits hier auf die Bedeutung klarer Eingabeaufforderungen hinweisen. (UI)
   
   * Merkblatt P2 “EVA-Prinzip”

   * Merkblatt P3 “Codegerüst I” inkl. Import-Anweisung für Bibliothek

   * Merkblatt P4 “Benutzerführung I” (mehr dann in Modul 322)

     

3. Bedeutung von Daten wieder am Beispiel Taschenrechner ausführen. Daraus einfache Datentypen ableiten. (int, float und char [für Operationszeichen]). Wertzuweisung im Programm thematisieren.

   * Merkblatt P5 “einfache Datentypen”

     

4. Einführen von Programmstrukturen (Sequenz, Verzweigung, Wiederholung) am Beispiel des Taschenrechners. Mit Mustercode einführen und an einfachen Beispielen vertiefen (alles noch ohne Funktionen/Methoden)
   Für die Iterationen (Kopf- und Fussgesteuert, mit Zählvariable oder Bedingung) passende Beispiele verwenden. Alles noch mit einfachen Bedingungen (<, <=, >, >=, ==< !=)
   
   * Merkblatt P6 “Programstrukturen”

   * Merkblatt P7 “Bedingungen I”

     

5. Programmkomplexität mittels Prozeduren und Funktionen (Methoden) aufbrechen. Einführung von Parameter und Rückgabewert. Wie immer zuerst am Beispiel des Taschenrechners.

   * Merkblatt P8 “Funktionen”

     

6. Umgang mit komplexen Datentypen (typischerweise String evtl. Array einfacher Datentypen?) und verknüpften Bedingungen (UND, ODER). Komplexere Beispiele mit z.B. Textbearbeitung (Textstellen erkennen, Zeichen ändern und/oder ersetzen usw.)

   * Merkblatt P9 “Bedingungen II”

   * Merkblatt P10 “komplexe Datentypen I”

     

7. Einführung einer IDE mit Debugger für die Fehlersuche. Die Anzahl und Komplexität der Übungen kann hier erhöht werden, so dass am Ende alle Vorgaben der Kompetenz-Matrix abgedeckt sind.

   * Merkblatt P11 “Compiler”

   * Merkblatt P12 “Debugger” (inkl. Fehlerbegriff)

     


## Lektionenplan

| Anzahl Lektionen | Themen                                                     | Kompetenzen | Tools                            | Learning Unit |
| ---------------- | ---------------------------------------------------------- | ----------- | -------------------------------- | ------------- |
|                  | Vorstellen des Moduls. Möglicher Ablauf. Notengebung usw.  |             | Concept Map zum Modul            |               |
|                  | Vom Kochrezept zur Ablaufdarstellung mit Activity-Diagramm |             | Musterlösung                     | LU01          |
|                  | Einfache Elemente des Activity-Diagramm und deren Nutzung  |             | Theorie, einfache Musterbeispiel | LU01          |
|                  | Umsetzung eines Ablaufs in Progammcode anhand Addition     |             | Merkblatt P1 “Activity-Diagramm” | LU01          |
|                  | Entwicklungsumgebung installieren                          |             | Anleitung JDK und Path           | LU02a         |
|                  | Mein ersten Java-Programm                                  |             | Template                         | LU02b         |
|                  | Nutzung einer Bibliothek für Werteingabe (Scanner)         |             |                                  |               |
|                  | Erweiterter Taschenrechner mit Grundfunktionen erstellen   |             |                                  |               |
|                  | Umgang mit Variablen und einfache Datentypen               |             |                                  |               |
|                  | Übungen zu einfachen Datenbearbeitungen                    |             |                                  |               |
|                  |                                                            |             |                                  |               |
|                  |                                                            |             |                                  |               |
|                  |                                                            |             |                                  |               |
|                  |                                                            |             |                                  |               |
|                  |                                                            |             |                                  |               |
|                  |                                                            |             |                                  |               |

