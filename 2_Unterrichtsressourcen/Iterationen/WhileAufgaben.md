# Umsetzungsvorschlag - Modul 319

## Aufgaben zu: while

Implementieren Sie folgende Aufgaben einmal mit Hilfe eines **while Loops**.

1. Alle Zahlen von 0 bis 20 ausgeben.

2. Alle Zahlen von 25 bis 50 ausgeben.

3. Alle Zahlen von -25 bis 25 ausgeben.



Implementieren Sie folgende Aufgaben mit Hilfe von **while-loops** aber ohne Multiplikation!

1. Alle Zahlen der 4-Reihe ausgeben. (4.. bis 40)

2. Alle Zahlen der 9 Reihe ausgeben. (9 … bis 81)

3. Alle Zahlen der 11 Reihe, beginnend mit der Zahl 44 bis 121, ausgeben
    

4. Alle Zahlen der 8 Reihe von 96 bis 16.

5. Alle Zahlen der 25 Reihe von 400 bis 100.

6. Alle Zahlen der 13 Reihe von 2197 bis 2028

   

## Aufgaben zu: do while

Implementieren Sie obigen Aufgaben mit Hilfe eines **do while Loops**.



## Aufgaben zu: mehrfache Schleife

Geben Sie das 1x1 bis 10x10 als Tabelle aus.
Verwenden Sie mehrere Schleifen.



## Aufgaben zu: logische Operatoren

Sie kennen sicher das Spiel, in dem man zum Beispiel keine Zahl mit 5 aussprechen darf.
Anstelle von 5 sagt man "Fizz".

" 1 2 3 4 Fizz 6 7 8 9 10 11 12 13 14 Fizz 16 17 usw."

Implementieren Sie das Spiel, verwenden Sie while Loops, Verzweigungen und logische Operatoren.

Hilfe: Der Modulo-Operator erlaubt es Ihnen zu prüfen, ob eine Zahl ohne Rest teilbar ist:

```
15%5 = 0, da 15 / 5 = 3 Rest 0
14%5= 4, da 14 / 5 = 2 Rest 4
```

Nun wird das Spiel erweitert. Neu muss man bei Zahlen mit einer 7 als "Buzz" aussprechen.

" 1 2 3 4 Fizz 6 Buzz 8 9 10 11 12 13 14 Fizz 16 Buzz usw."