import java.util.Scanner;
 
/**
 * short description of this program
 * 
 * @author Ibid umm
 * @since yyyy-mm-dd
 * @version 1.0
 */
public class Template {
 
    // declare attributes
    static Scanner scanner;
 
    /**
     * constructor: initialize attributes
     */
    public Template() {
        scanner = new Scanner(System.in);
    }
 
    /**
     * starts the execution
     * 
     * @param args
     *            command line arguments
     */
    public static void main(String[] args) {
        Template program = new Template();
        program.run();
        scanner.close();
    }
 
    /**
     * description
     */
    private void run() {
        // TODO: realize the program logic
    }
}
