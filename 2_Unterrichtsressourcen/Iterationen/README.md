# Umsetzungsvorschlag - Modul 319

## Iterationen: while (while loop, while Schleife)

#### Aktivitätendiagramm  

<img height=500 lt="image-20210425144209431" src="x_gitressourcen/Images/README/image-20210425144209431.png">



#### Programm Code
<img height=200 lt="image-20210425144845457" src="x_gitressourcen/Images/README/image-20210425144845457.png">  



#### Erläuterung

| Code                  | Erklärung                                                    |
| --------------------- | ------------------------------------------------------------ |
| **while (Bedingung)** | Iteration mit Bedingung für Abbruch der Wiederholung         |
| {                     | Block Umschliesst Sequenz, die ausgeführt                    |
| /* Anweisungen */     | Anweisungen, die bei jeder Wiederholung ausgeführt wird, solange Bedingung erfüllt ist |
| }                     |                                                              |



## Iterationen: dowhile (while loop, while Schleife)

#### Aktivitätendiagramm

<img height=500 lt="image-20210425145453539" src="x_gitressourcen/Images/README/image-20210425145453539.png">  

#### Programm Code

<img height=200 lt="image-20210425145531929" src="x_gitressourcen/Images/README/image-20210425145531929.png">



#### Erläuterung

| Code                      | Erklärung                                                    |
| ------------------------- | ------------------------------------------------------------ |
| **do**                    | Iteration Beginn                                             |
| {                         | Block Umschliesst Sequenz, die ausgeführt                    |
| /* Anweisungen */         | Anweisungen, die bei jeder Wiederholung ausgeführt wird, solange Bedingung erfüllt ist |
| }  **while (Bedingung);** | Bedingung für Abbruch der Wiederholung                       |



## Bedingungen und logische Operatoren

#### Not

Mittels **NOT** Verknüpfungen lassen sich Bedingungen ‚umgekehrt‘ auswerten:

| Code                | Erklärung                                     |
| ------------------- | --------------------------------------------- |
| while( ! Bedingung) | Wahr, wenn die Bedingungen **nicht** wahr ist |

#### AND, OR

Mittels **logischer Verknüpfungen** lassen sich Bedingungen ‚verkettet‘ auswerten:

| Code                               | Erklärung                                      |
| ---------------------------------- | ---------------------------------------------- |
| while( Bedingung1 AND Bedingung2 ) | Wahr, wenn beide Bedingungen wahr sind         |
| while( Bedingung1 OR Bedingung2 )  | Wahr, wenn mindestens eine Bedingung wahr ist. |

#### Klammern

Mittels **Klammern** kann die Auswertung der Bedingungen zusätzlich definiert werden:

| Code                                                  | Erklärung                                                    |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| while( Bedingung1 AND ( Bedingung2 OR Bedingung3 )  ) | Zuerst wird Bedingung2 mit Bedingung3<br />verknüpft. Dann das Resultat mit der <br />Bedingung1 verknüpft. |