# Umsetzungsvorschlag - Modul 319

## Lektionenplan

(Auf 2 x 45' Lektionenblöcke normiert)

Allgemeine Kompetenzen, die in verschiedenen Lektionen zum Tragen kommen und deshalb nicht spezifisch überall auch noch aufgeführt werden:
AG1, AF1, AE1
EG1, EF1, EE1
FG1, FF1, FE1

| Anzahl Lektionen | Themen                                                       | Kompetenzen                                           | Tools                |
| ---------------- | ------------------------------------------------------------ | ----------------------------------------------------- | -------------------- |
| 2                | **Modul einführen**<br />Übersicht über das Modul geben.<br /> (zBsp Kompetenzen, Inhalte, Bewertung...) <br /><br />**Abläufe darstellen:**<br />Einen Ablauf als eine Sequenz von Aktivitäten, Verzweigungen und Wiederholungen verstehen. <br />Einen dargestellten Ablauf interpretieren:<br />Start- und Ende, Aktivität, Verzweigung, Wiederholung... <br / Aus einer Beschreibung einen einfachen Ablauf grafisch darstellen. | AG2 AF2 DG1, DF1, DE1, FG1                            | zBsp UML Tool        |
| 2                | **Abläufe darstellen vertiefen**<br />Symbolik der Darstellung repetieren.<br />Interpretation von dargestellten Abläufen vertiefen.<br />Beschriebene Abläufe darstellen vertiefen.<br />Unterschiede aus Beschreibung und dargestelltem Ablauf finden und Verbesserungen vorschlagen. | AG2 AF2, AE2, DG1, DF1, DE1, FG1                      | zBsp UML Tool        |
| 2                | **Entwicklungsumgebung**<br />Entwicklungsumgebung einrichten.<br />Erstes Hello World programmieren.<br />**Variablen und Verzweigung anwenden**<br />Einen vorgegebenen Ablauf in ein einfaches Programm umsetzen oder ein vorbereitetes Programm erweitern. Erste Datentypen anwenden (Ganzzahl, Gleitkommazahl..) Variablen deklarieren, initialisieren und  Werte zuweisen. <br />Verzweigung implementieren. | EG1, EF1, EE1<br /><br />BG1, BF1, BG2, BF3, BG3, BF4 | Entwicklungsumgebung |
| 2                | **Variablen und Verzweigung vertiefen**<br />Einen vorgegeben Ablauf in ein einfaches Programm umsetzen oder ein vorbereitetes Programm erweitern. Datentypen anwenden (Ganzzahl, Gleitkommazahl..) Variablen deklarieren, initialisieren und  Werte zuweisen. <br />Verzweigung implementieren.<br /> **Teilbedingungen mit AND  oder OR anwenden**<br />Verzweigungen mit AND oder OR anwenden | BG1, BF1, BG2, BF3, BE2, BE3, DE2                     | Entwicklungsumgebung |
| 2       10       | **Iterationen anwenden**<br />Kopf- und Fuss-gesteuerte Iterationen umsetzen. | FG3, FF3                                              | Entwicklungsumgebung |
| 2                | **Iterationen vertiefen**<br />Kopf- und Fuss-gesteuerte Iterationen umsetzen. | FF3, FE3                                              | Entwicklungsumgebung |
| 2                | **Zusammengesetzte Datentypen**<br />Zusammengesetzte Datentypen deklarieren, initialisieren und zuweisen.<br />**Anwenden von for und foreach**<br />For und foreach in Zusammenhang mit den Zusammengesetzten Datentypen anwenden. | CG1, CF1 FF3, FE3                                     | Entwicklungsumgebung |
| 2                | **Zusammengesetzte Datentypen und for, foreach vertiefen**<br />Weitere Iterationen umsetzen | CF1, CG1                                              | Entwicklungsumgebung |
| 2                | **Compiler verstehen und Debugger anwenden**<br />Funktion von Compiler und Debugger verstehen.<br />Fehler- und Warnmeldungen interpretieren, Fehler beheben. Den Compiler für die Analyse einsetzen. | EG2, EF2, EE2, EG3, EF3, EE3                          | Entwicklungsumgebung |
| 2     20         | **Klassen, Objekte, Instanzen und Instanzvariablen**<br />Begriffe verstehen und in einem Programm anwenden können. <br />**Methoden anwenden**<br />Methoden auf Objekte anwenden können.<br />Return Werte auswerten | BF2, BE1, FG2, FF2 FE2                                | Entwicklungsumgebung |
| 2                | **Methoden mit Parametern anwenden**<br />Methoden auf Objekte anwenden können.<br />Parameter übergeben, Return Werte auswerten | BF2, BE1, FG2, FF2 FE2                                | Entwicklungsumgebung |
| 2                | **Methoden mit Parametern vertiefen**                        | BF2, BE1, CE1                                         | Entwicklungsumgebung |
| 2                | **Konventionen einhalten**<br />Formatierungsmöglichkeiten der IDE. <br />Formatierungen bei Kommentaren, CleanCode.<br />Bestehendes Programm verbessern. | GG1, GF1, GE1                                         | Entwicklungsumgebung |
| 2                | **Anwendung von allen Teilen vertiefen**<br />Weitere Übungen lösen.<br />Vorgegeben Abläufe kritisch hinterfragen und verbessern | AF1, AE1, AE2                                         | Entwicklungsumgebung |
| 2    30          | **Anwendung von allen Teilen vertiefen**<br />Weitere Übungen lösen.<br />Vorgegeben Abläufe kritisch hinterfragen und verbessern | AF2, AE2                                              | Entwicklungsumgebung |
| 2                | **Anwendung von allen Teilen vertiefen**<br />Weitere Übungen lösen.<br />Vorgegeben Abläufe kritisch hinterfragen und verbessern | AF2, AE2                                              | Entwicklungsumgebung |
| 2                | Zeit für Kompetenznachweise, verteilt in den oberen Blöcken  |                                                       |                      |
| 2                | Zeit für Kompetenznachweise, verteilt in den oberen Blöcken  |                                                       |                      |
| 2                | Reserve nicht verplanen                                      |                                                       |                      |
| 2   40           | Reserve nicht verplanen                                      |                                                       |                      |

