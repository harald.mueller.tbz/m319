# Kompetenzmatrix - Modul 319

| Kompetenzband:                                         | HZ   | Grundlagen                                                   | Fortgeschritten                                              | Erweitert                                                    |
| ------------------------------------------------------ | ---- | :----------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Probleme erfassen und Lösungsansätze entwickeln.       | 1    | AG1: Ich kenne die Eigenschaften von gut formulierten Anforderungen | AF1: Ich kann Anforderungen erfassen und erläutern.          | AE1: Ich kann einfache Anforderungen formulieren.            |
|                                                        |      | AG2: Ich kann eine strukturierte  Vorgehensweise für das Entwickeln einer Lösung aufzeigen | AF2: Ich kann eine strukturierte  Vorgehensweise für das Entwickeln einer Lösung anwenden. | AE2: Ich kann Vorgehensweise kritisch hinterfragen und Verbesserungsvorschläge nennen. |
| Daten, Datentypen und Variablen ableiten und einsetzen | 2,4  | BG1: Ich kann die Unterschiede von Datentypen erklären. (Bsp: Ganzzahlen, Gleitkommazahlen, Zeichen usw.) | BF1: Ich kann den richtigen  Datentyp für eine Variable aufgrund der Aufgabenstellung wählen. | BE1: Ich kann komplexe Datentypen (zBsp String, Integer, Double...) aufgrund der Aufgabenstellung wählen. |
|                                                        |      |                                                              | BF2: Ich kann den Unterschied zwischen primitiven und komplexen Datentypen erklären. |                                                              |
|                                                        |      | BG2: Ich den Zweck von Variablen in einem Programm erläutern. | BF3: Ich kann Variablen deklarieren, initialisieren und Zuweisungen vornehmen. | BE2: Ich kann Variablen als Konstanten deklarieren und initialisieren. |
|                                                        |      | BG3: Ich kann die Möglichkeit der Verwendung von Variablen abhängig von Ihrem Datentyp erläutern. | BF4: Ich kann die Variablen abhängig von Ihrem Datentyp anwenden. (ZBps für arithmetische Operation) | BE3: Ich kann Variablen mit einem bestimmten Datentyp auf einen anderen umwandlen.(zBps: toString) |
| Zusammengesetze Datentypen einsetzen                   | 2    | CG1: Ich kann den Zweck von zusammengesetzten Datentypen erläutern. (ZBsp eindimensionaler Array) | CF1: Ich kann zusammengesetzte Datentypen deklarieren, initialisieren und Zuweisungen vornehmen. (ZBsp eindimensionaler Array) | CE1: Ich kann Methoden der zusammengesetzten Datentypen anwenden. |
| Anforderungen visuell darstellen                       | 3    | DG1: Ich kenne die Einsatzgebiete der grafischen Beschreibung eines Ablaufes. (zBsp mit Activity-Diagramm, Sequenz-Diagramm..) | DF1: Ich kann einen vorgegebenen Programmablauf grafisch darstellen. (zBsp Activity-Diagramm, Sequenz-Diagramm..) | DE1: Ich kann beschriebenen Ablauf in einen Programmablauf überführen und grafisch darstellen.(zBsp Activity-Diagramm, Sequenz-Diagramm..) |
|                                                        |      |                                                              |                                                              | DE2: Ich kann Bedingungen mit mehr als zwei Teilbedingungen formulieren. |
| Entwicklungsumgebung einsetzen                         | 4,6  | EG1: Ich kann den Zweck einer  Entwicklungsumgebung (IDE) erklären. | EF1:  Ich kann die Entwicklungsumgebung effizient einsetzen  | EE1: Ich kann die Shortcuts und Funktionen der Entwicklungsumgebung effizient einsetzen |
|                                                        |      | EG2: Ich kann erläutern für was das Compilieren dient.       | EF2: Ich kann vom Compiler angezeigte Fehler- und Warnmeldung interpretieren. | EE2: Ich kann die Ursachen der vom Compiler angezeigten Fehler- und Warnmeldungen beheben. |
|                                                        |      | EG3: Ich kann erläutern für was ein Debugger dient.          | EF3: Ich kann einen Debugger zur Programmausführung anwenden. | EE3: Ich kann einen Debugger die Fehleranalyse einsetzen.    |
| Applikation implementieren                             | 3,4  | FG1: Ich kann Aufbau, Syntax und Struktur eines einfachen Programmes erklären. | FF1: Ich kann einen detailliert vorgegeben Ablauf mit einer Programmiersprache umsetzen. | FE1: Ich kann einen grob beschriebenen Ablauf detaillieren und mit einer Programmiersprache umsetzen. |
|                                                        |      | FG2: Ich kann erklären wieso die Aufteilung eines Programmes in verschiedene Methoden sinnvoll ist. | FF2: Ich kenne den Aufbau und den Aufruf einer Methode (Deklaration und Implementation) und kann diese korrekt einsetzen. (zBsp Instanzvariablen, Parametern, lokalen Variablen, Return Werte) | FE2: Ich kann Instanzvariablen, Parametern, lokalen Variablen und Return Werte gezielt einsetzen. |
|                                                        |      | FG3: Ich kann Selektionen und Iterationen (Kopf und Fussgesteuert) mit Bedingungen codieren. | FF3: Ich kann zwei Teilbedingungen mit AND oder OR verknüpfen. | FE3: Ich kann verschachtelte Kontrollstrukturen einsetzen.   |
| Konventionen einhalten                                 | 5    | GG1: Ich kann mein Programm mit ein- und mehrzeiligen Kommentaren ergänzen. | GF1: Ich kenne Möglichkeiten Kommentare zu Formatieren oder zu Annotieren (zBsp FIXME, TODO etc.) | GE1: Ich setze Konventionen ein. (zBsp Clean Code, Coding Guidelines..) |

## Kompetenzstufen

### Grundlagen | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.0*

### Fortgeschritten | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.5*

### Erweitert | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*

## Fragekatalog

Link zum [Fragekatalog](Fragenkatalog.md)

